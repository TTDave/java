import java.util.Scanner;


public class helloWorld {
    public static void main(String[] args) {
        // Prints "Hello, World" to the terminal window.
        System.out.println("Hello, World");
        
        Scanner reader = new Scanner(System.in);  // Reading from System.in
        
        System.out.println("Enter first number: ");
        int n = reader.nextInt(); // Scans the next token of the input as an int.
        
        System.out.println("Enter secound number: ");
        int y = reader.nextInt(); // Scans the next token of the input as an int.
        
        //once finished
        reader.close();
        
        
        System.out.println(n + " + " + y + " = " + (n + y) );
        System.out.println(n + " - " + y + " = " + (n - y) );
        System.out.println(n + " * " + y + " = " + (n * y) );
        System.out.println(n + " / " + y + " = " + (n / y) );
    }
}
