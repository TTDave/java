// TODO rewrite it so that the whole sequence gets returned as a string
public class Fibbonacci {
	// A variable vissible in all methods
	private static int max;

	public static void main(String[] args) {
		max = 10;
		fibbonacci(0, 1, 0);
		// System.out.print(fibonacci(6));
	}
    // a method that writes the fibonacci sequence for x number of numbers
	private static int fibbonacci(int n1, int n2, int counter) {
        // Base case
        if (max == counter) {
			return 0;
		} else {
            // printes the number + a whitespace
			System.out.print(n1 + " ");
            // returns number 2 i 1s place and makes the 2nd place 1 + 2 = 3 and so on and so on
			return fibbonacci(n2, n1 + n2, counter + 1);
		}

	}
    // someone elses code i found that is much cleaner but it only returns the n:th number from the sequence
	public static int fibonacci(int n) {
		if (n == 0)
			return 0;
		else if (n == 1)
			return 1;
		else
			return fibonacci(n - 1) + fibonacci(n - 2);
	}

}
