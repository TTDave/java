import java.util.Scanner;
import java.util.Random;

public class GuessingGame{

    public static void main(String[] args) {
    	
    	// Defines all variables that are used
        int random, guess, attempts;
        
        // defines the sacanner
        Scanner keyboard = new Scanner(System.in);
        
        // gives a random value to generator
        Random generator = new Random();
        random = generator.nextInt(100) + 1;
        attempts = 1; 

        System.out.print("I am thinking of a number between 0 and 100, what do you think it is?\n");
        
        // sets the value of guess to the next user input
        guess = keyboard.nextInt(); 
        while (guess != random) {
            if (guess > random) {
                System.out.print("Lower!\n");
                attempts += 1; 
            }
            else {
                System.out.print("Higher!\n");
                attempts +=1;
            }
         guess = keyboard.nextInt();
        }
        // Closes the user input scanner
        keyboard.close();

        System.out.print(random + " is the correct answer and it took you " + attempts + " attempts to guess it!");

    }        
}