
package main;

public enum Suit {
    Hearts,
    Spades,
    Clubs,
    Diamonds
}