/*
 * CARD RANKING
 * 5 of a kind
 * Straight royal flush
 * Straight flush
 * 4 of a kind
 * full house
 * flush
 * straight
 * 3 of a kind
 * 2 pair
 * pair 
 * high card
 * 
 * CARD CHECKING
 * 
 * all pair just check how many of same cards are in hand and put it in a variable
 * and use it to determine how many you got
 * 
 * for make pre defined patterns
 * a 2 3 4 5 ... j q k (ACE LOW)
 * 2 3 4 5 ... j q k a (ACE HIGH)
 * and test your hand if the same pattern is found in either one of the pre defined patterns
 * 
 * for flush check if all cards have same suit and if you have joker just make the suit of the joker the
 * suit wich you have most cards of
 * 
 * full house test if you have a pair of 3 and a pair of 2 using the pair variable
 * 
 * 2 pair same as above but 2 pairs of 2
 * 
 * high card check first if you either have ace king queen or jack if not see wich card has higest number
 * 
 * If you have joker so in flush just test joker as all possible values starting from ace going to 2
 * 
 * For pairs it should work to try for 3 of a kind as example
 * if you have 2 in pair and 1 in joker then you get 3 of a kind and to avoid making annoyingly long code
 * and to avoid making annoyingly long code to decied wich card you use for a pair start by not saying in wich card
 * you got a pair
 * 
 * Later either make it so that get the best possible combo or if its not possible just skip using JOKER alltogether
 * 
 * SAVING
 * 
 * Do exatly like in python poker it firsts test if you have a local database if you have one it puts all values in a row in the
 * database if you dont have a database it creates a sqlite database with and a table
 * 
 * Values that should be saved
 * All current cards in hand (unless the cards have allready been played and not yet redrawn)
 * money that the player has (attribute money not yet present)
 * And if you havnt yet played the cards you should save card deck (Might be very hard if its not doable try to save the first 
 * 5 cards in the deck sine you dont need more then them)
 * 
 * 2 player mode
 * 
 * Try to make it as realistic ass possible if its to hard just make it somewhat realistic
 * 
 * 
 */
/*
 * TODO
 * Remake so Suit.java is no longer needed
 * Find out a better way to change cards DONE
 * Make code smaller 
 * Make card changer med multichoice checkboxes then dupe checker wont be neccesary
 */

package main;

import javax.swing.JOptionPane;

public class Main {
	public static void main(String args[]) {
		String output;
		Deck deck = new Deck();
		Player player1 = new Player(); // Add attribute to player money
		while (true) {
			// add here choice for long name or shortname
			// före loopen

			// System.out.println(deck);
			// System.out.println("Size of deck is: " + deck.getSizeOfDeck());
			deck.shuffle();
			// System.out.println("Deck after shuffling is " + deck);
			for (int x = 0; x <= 4; x++) {
				deck.dealCard(player1);
				// System.out.println(deck);
				// System.out.println("Size of deck after dealing a card to player is " +
				// deck.getSizeOfDeck());
			}
			// System.out.println("Size of deck after dealing a card to player is " +
			// deck.getSizeOfDeck());
			// System.out.println("Size of deck after dealing a card to player is " +
			// deck.getSizeOfDeck());
			// System.out.println("Hand of player 1 is " + player1.getHand());
			// System.out.println("Hand of player 1 is " +
			// player1.getHand().get(1).getName());
			output = "Choose which cards to change: (use 1,2,3,4,5)\n";
			for (int x = 0; x <= player1.getHand().size() - 1; x++) {
				String message = x + 1 + " ) " + player1.getHand().get(x).getName() + "\n";
				output += message;
			}
			/*
			 * String[] buttons = { "1", "2", "3", "4","5" ,"DONE"}; int returnValue =
			 * JOptionPane.showOptionDialog(null, output, "Change cards",
			 * JOptionPane.WARNING_MESSAGE, 0, null, buttons, buttons[0]);
			 * System.out.println(returnValue); if (returnValue == 5) { break;
			 */
			// Make the input dialog en box med multichoice checkbox
			String userInput = JOptionPane.showInputDialog(output);
			if (userInput == null) break;
			// REMOVE UNVANTED CHARACTERS
			userInput = userInput.replaceAll("[^1-5]", "");

			char[] characters = userInput.toCharArray();
			boolean[] found = new boolean[256];
			StringBuilder sb = new StringBuilder();
			// System.out.println("String with duplicates : " + userInput);
			for (char c : characters) {
				if (!found[c]) {
					found[c] = true;
					sb.append(c);
				}
			}
			// System.out.println("String after duplicates removed : " + sb.toString());
			String noDublicatesInput = sb.toString();
			for (int playerCardIndex = 0; playerCardIndex <= 4; playerCardIndex++) {
				for (char character : noDublicatesInput.toCharArray()) {
					// Tests if the current index of userInput contains the current
					// playerCardIndex+1
					if (String.valueOf(character).contains(Integer.toString(playerCardIndex + 1))) {
						// Removes the card you want to remove
						player1.getHand().remove(playerCardIndex);
						// Adds the new card to the index of choice
						player1.getHand().add(playerCardIndex, deck.dealCard());
					}
				}
			}
			// LEGACY CODE
			for (int x = 0; x <= 4; x++) {
				System.out.println(x + 1 + " ) " + player1.getHand().get(x).getName());
			}
			// MAKE MESSAGE VARIABLE DEFINED IN FOR LOOP SÅ DEN SEN DIE

			output = "";
			for (int x = 0; x <= 4; x++) {
				String message = x + 1 + " ) " + player1.getHand().get(x).getName() + "\n";
				output += message;
			}
			JOptionPane.showMessageDialog(null, output);

			// HERE IT SHOULD CHECK WHAT YOU GOT
			String hand = "";
			for (int x = 0; x <= 4; x++) {
				hand += player1.getHand().get(x).getshortName()+" ";
			}
			for (String input : new String[] { hand }) {
				JOptionPane.showMessageDialog(null,PokerHandAnalyzer.analyzeHand(input.split(" ")));
				// REMOVES THE HAND AFTER ROUND IS OVER
				
			}
			player1.getHand().removeAll(player1.getHand());
		}
	}
}