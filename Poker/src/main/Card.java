package main;
//Each card has a value and a suite.
public class Card {
	String value;
	Suit suit;
	String name;
	String shortName;
	
	public Card(String value, Suit suit,String name, String shortName) {
		this.value = value;
		this.suit = suit;
		this.name = name;
		this.shortName = shortName;
		}
	
	public String getshortName() {
		return shortName;
		}
	public String getValue() {
		return value;
		}
	
	public String getName() {
		return name;
		}
	
	public Suit getSuit() {
		return suit;
        }
	
	public void setValue(String value) {
		this.value = value;
		}
	
	public void setSuit(Suit suit) {
		this.suit = suit;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	@Override
	public String toString() {
		return name;
		}
	}