package main;    
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Deck {
    public List<Card> cardDeck;

    public Deck() {
        this.cardDeck = new ArrayList<Card>();
        String name;
        String shortName;
		String[] names = new String[] {"Ace","2", "3", "4", "5", "6", "7", "8", "9", "10", "Jack", "Queen", "King"};
		String[] faces = new String[] {"A","2", "3", "4", "5", "6", "7", "8", "9", "T", "J", "Q", "K"};
        for(int value = 0 ; value <= 12 ; value++){
            for(Suit suit : Suit.values()){
            	name = names[value]+" of "+suit; 	
            	shortName = faces[value] + suit.toString().charAt(0);
            	cardDeck.add(new Card(faces[value],suit,name,shortName));
            }
        }
    }

    @Override
    public String toString() {
        return "Deck{" +
                "cardDeck=" + cardDeck +
                '}';
    }

    public void shuffle(){
    	
    	Collections.shuffle(cardDeck);
        
    }
// https://stackoverflow.com/questions/20186681/how-to-move-specific-item-in-array-list-to-the-first-item
    public void dealCard(Player player){
       //Get next card and add to hand of the player
        Card removedCard = cardDeck.remove(0);
        cardDeck.add(51, removedCard);
        player.getHand().add(removedCard);
    }
    // FIND OUT VILKEN AV DOM HÄR SOM ANVÄNDS
    public Card dealCard(){
        Card removedCard = cardDeck.remove(0);
        cardDeck.add(51, removedCard);
        return removedCard;
    }

    //Size of the deck for testing purpose
    public int getSizeOfDeck(){
        return cardDeck.size();
    }
}